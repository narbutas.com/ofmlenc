package ofmlenc

import (
	"errors"
	"strings"

	"golang.org/x/text/encoding"
	"golang.org/x/text/encoding/charmap"
)

// Encoding contains an encoding and encoder / decoder pair
type Encoding struct {
	encoding     *charmap.Charmap
	extraLetters string
	Encoder      *encoding.Encoder
	Decoder      *encoding.Decoder
}

// Encodings is a map from language to Encoding
var Encodings = map[string]Encoding{
	"":   {encoding: charmap.Windows1252},
	"en": {encoding: charmap.Windows1252},
	"us": {encoding: charmap.Windows1252},
	"de": {encoding: charmap.Windows1252, extraLetters: "äöüß"},
	"fr": {encoding: charmap.Windows1252, extraLetters: "àâæçéèêëîïôœùûüÿ"},
	"lt": {encoding: charmap.Windows1257, extraLetters: "ąčęėįšųūž"},
	"ru": {encoding: charmap.Windows1251, extraLetters: "абвгдеёжзийклмнопрстуфхцчшщъыьэюя"},
	"es": {encoding: charmap.Windows1252, extraLetters: "áéíóúñü"},
}

// encoderCandidates are encodings used for guessing the encoding used when saving (borking UTF-8)
// List taken from https://html.spec.whatwg.org/multipage/parsing.html#determining-the-character-encoding
// ordered by prevalence / relevance to our languages
var encoderCandidates = []*encoding.Encoder{
	charmap.Windows1252.NewEncoder(),
	charmap.Windows1251.NewEncoder(),
	charmap.Windows1257.NewEncoder(),
	charmap.Windows1250.NewEncoder(),
	charmap.ISO8859_2.NewEncoder(),
	// charmap.Windows1256.NewEncoder(),
	// charmap.Windows1254.NewEncoder(),
	// Could also use windows-874, windows-1258, windows-1255, Shift_JIS, ISO-8859-7, gb18030, EUC-KR, Big5
}

// Languages contains supported languages in default order
var Languages = []string{"en", "de", "fr", "lt", "ru", "us", "es"}

func init() {
	for lang, te := range Encodings {
		te.Encoder = te.encoding.NewEncoder()
		te.Decoder = te.encoding.NewDecoder()
		te.extraLetters += strings.ToUpper(te.extraLetters)
		Encodings[lang] = te
	}
}

// ErrUnsupportedLang is returned by Encode and Decode when unknown language is requested
var ErrUnsupportedLang = errors.New("Language not supported")

// Encode encodes the given text from UTF-8 to the correct OFML encoding for the given language (which is still UTF-8, just borked)
func Encode(lang, text string) (string, error) {
	enc, ok := Encodings[lang]
	if !ok {
		return text, ErrUnsupportedLang
	}
	coded, err := enc.Encoder.String(text) // turn to required encoding
	if err != nil {
		return text, err
	}
	return Encodings[""].Decoder.String(coded) // pretend coded string is in default single-byte encoding and turn it to UTF-8
}

// Decode decodes the given text from specific language OFML encoding (borked UTF-8) to normal UTF-8
// Decode only supports decoding from files saved in borked Windows1252 encoding (for example from box.pcon-solutions.com)
// For full decoding support use DecodeMap, as more than one language can be used to determine used encoding
func Decode(lang, text string) (string, error) {
	enc, ok := Encodings[lang]
	if !ok {
		return text, ErrUnsupportedLang
	}
	coded, err := Encodings[""].Encoder.String(text) // encode our borked UTF-8 to default single-byte encoding
	if err != nil {
		return text, err
	}
	return enc.Decoder.String(coded) // decode coded string to UTF-8, as if it always was in lang's single-byte encoding
}

// DecodeMap decodes the given OFML encoded text (borked UTF-8) in all supported languages at once
// The important thing is to correctly determine (guess) what encoding was used when borking ("saving") the text
func DecodeMap(input map[string]string) map[string]string {
	maxDecodedCount := 0
	var maxDecodedResult map[string]string
	for _, encoderCandidate := range encoderCandidates {
		ret := make(map[string]string)
		decodedCount := 0
		for lang, text := range input {
			if _, found := Encodings[lang]; !found {
				continue
			}
			coded, err := encoderCandidate.String(text)
			if err != nil {
				decodedCount = 0
				break
			}
			decoded, err := Encodings[lang].Decoder.String(coded)
			if err != nil {
				decodedCount = 0
				break
			}
			ret[lang] = decoded
			for _, r := range decoded {
				if r >= 'A' && r <= 'Z' || r >= 'a' && r <= 'z' {
					decodedCount++
				}
				for _, r2 := range Encodings[lang].extraLetters {
					if r == r2 {
						decodedCount++
						break
					}
				}
			}
		}
		if decodedCount == 0 {
			continue
		}
		if decodedCount > maxDecodedCount {
			maxDecodedCount = decodedCount
			maxDecodedResult = ret
		}
	}

	if maxDecodedCount > 0 {
		return maxDecodedResult
	}

	// use default encoding
	ret := make(map[string]string)
	for lang, text := range input {
		if _, found := Encodings[lang]; !found {
			continue
		}
		coded, err := Encodings[""].Encoder.String(text)
		if err != nil {
			continue
		}
		decoded, err := Encodings[lang].Decoder.String(coded)
		if err != nil {
			continue
		}
		ret[lang] = decoded
	}
	return ret
}
