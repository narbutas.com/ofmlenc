package ofmlenc_test

import (
	"testing"

	"gitlab.com/narbutas.com/ofmlenc"
)

func TestDecodeMapWindows1252(t *testing.T) {
	encoded := map[string]string{
		"en": "Visitor chair. Four legged with castors. Seat upholstered with fabric, back-rest with mesh. Steel frame can be chromed or powder-coated. Integrated armrests with plastic finish.; (W=570, D=600, H=865, SH=465)",
		"de": "Besucherstuhl. Vierbein mit Rollen. Sitz gepolstert mit Stoff, Rückenlehne mit Mesh. Stahlrahmen kann verchromt oder pulverbeschichtet sein. Integrierte Armlehnen mit Kunststoffauflagen.; (W=570, D=600, H=865, SH=465)",
		"fr": "Siège visiteur. 4 pieds avec roulettes. Assise recouverte de tissu, dossier recouvert de maille. La structure en acier peut être chromée ou revêtue à la poudre. Accoudoirs intégrés avec finitions en plastique.; (W=570, D=600, H=865, SH=465)",
		"lt": "Lankytojo këdë su keturiomis kojomis ir ratukais. Sëdimoji dalis aptraukta gobelenu, atloðas - tinkleliu. Plieninis rëmas gali bûti chromuotas arba daþytas. Porankiø virðus padengtas juodos spalvos plastiko padeliais. ; (W=570, D=600, H=865, SH=465)",
		"ru": "Ñòóë äëÿ ïîñåòèòåëåé íà 4 ìåòàëëè÷åñêèõ íîæêàõ  íà êîëåñèêàõ. Ñèäåíèå îáòÿíóòî ãîáåëåíîì, èñêóññòâåííîé èëè íàòóðàëüíîé êîæåé, ñïèíêà - ñåòêîé. Ñòàëüíàÿ ðàìà ìîæåò áûòü õðîìèðîâàííàÿ èëè êðàøåííàÿ. Âåðõíÿÿ ÷àcòü ïîäëîêîòíèêîâ ïîêðûòà íàêëàäêîé èç ÷åðíîãî ïëàñòèêà.; (W=570, D=600, H=865, SH=465)",
		"us": "Visitor chair. Four legged with castors. Seat upholstered with fabric, back-rest with mesh. Steel frame can be chromed or powder-coated. Integrated armrests with plastic finish.; (W=570, D=600, H=865, SH=465)",
	}
	expected := map[string]string{
		"de": "Besucherstuhl. Vierbein mit Rollen. Sitz gepolstert mit Stoff, Rückenlehne mit Mesh. Stahlrahmen kann verchromt oder pulverbeschichtet sein. Integrierte Armlehnen mit Kunststoffauflagen.; (W=570, D=600, H=865, SH=465)",
		"en": "Visitor chair. Four legged with castors. Seat upholstered with fabric, back-rest with mesh. Steel frame can be chromed or powder-coated. Integrated armrests with plastic finish.; (W=570, D=600, H=865, SH=465)",
		"fr": "Siège visiteur. 4 pieds avec roulettes. Assise recouverte de tissu, dossier recouvert de maille. La structure en acier peut être chromée ou revêtue à la poudre. Accoudoirs intégrés avec finitions en plastique.; (W=570, D=600, H=865, SH=465)",
		"lt": "Lankytojo kėdė su keturiomis kojomis ir ratukais. Sėdimoji dalis aptraukta gobelenu, atlošas - tinkleliu. Plieninis rėmas gali būti chromuotas arba dažytas. Porankių viršus padengtas juodos spalvos plastiko padeliais. ; (W=570, D=600, H=865, SH=465)",
		"ru": "Стул для посетителей на 4 металлических ножках  на колесиках. Сидение обтянуто гобеленом, искусственной или натуральной кожей, спинка - сеткой. Стальная рама может быть хромированная или крашенная. Верхняя чаcть подлокотников покрыта накладкой из черного пластика.; (W=570, D=600, H=865, SH=465)",
		"us": "Visitor chair. Four legged with castors. Seat upholstered with fabric, back-rest with mesh. Steel frame can be chromed or powder-coated. Integrated armrests with plastic finish.; (W=570, D=600, H=865, SH=465)",
	}
	decoded := ofmlenc.DecodeMap(encoded)
	checkMapsEqual(t, decoded, expected)
}

func TestDecodeMapWindows1251(t *testing.T) {
	encoded := map[string]string{
		"en": "Visitor chair. Four legged with castors. Seat upholstered with fabric, back-rest with mesh. Steel frame can be chromed or powder-coated. Integrated armrests with plastic finish.; (W=570, D=600, H=865, SH=465)",
		"de": "Besucherstuhl. Vierbein mit Rollen. Sitz gepolstert mit Stoff, Rьckenlehne mit Mesh. Stahlrahmen kann verchromt oder pulverbeschichtet sein. Integrierte Armlehnen mit Kunststoffauflagen.; (W=570, D=600, H=865, SH=465)",
		"fr": "Siиge visiteur. 4 pieds avec roulettes. Assise recouverte de tissu, dossier recouvert de maille. La structure en acier peut кtre chromйe ou revкtue а la poudre. Accoudoirs intйgrйs avec finitions en plastique.; (W=570, D=600, H=865, SH=465)",
		"lt": "Lankytojo kлdл su keturiomis kojomis ir ratukais. Sлdimoji dalis aptraukta gobelenu, atloрas - tinkleliu. Plieninis rлmas gali bыti chromuotas arba daюytas. Porankiш virрus padengtas juodos spalvos plastiko padeliais. ; (W=570, D=600, H=865, SH=465)",
		"ru": "Стул для посетителей на 4 металлических ножках  на колесиках. Сидение обтянуто гобеленом, искусственной или натуральной кожей, спинка - сеткой. Стальная рама может быть хромированная или крашенная. Верхняя чаcть подлокотников покрыта накладкой из черного пластика.; (W=570, D=600, H=865, SH=465)",
		"us": "Visitor chair. Four legged with castors. Seat upholstered with fabric, back-rest with mesh. Steel frame can be chromed or powder-coated. Integrated armrests with plastic finish.; (W=570, D=600, H=865, SH=465)",
	}
	expected := map[string]string{
		"de": "Besucherstuhl. Vierbein mit Rollen. Sitz gepolstert mit Stoff, Rückenlehne mit Mesh. Stahlrahmen kann verchromt oder pulverbeschichtet sein. Integrierte Armlehnen mit Kunststoffauflagen.; (W=570, D=600, H=865, SH=465)",
		"en": "Visitor chair. Four legged with castors. Seat upholstered with fabric, back-rest with mesh. Steel frame can be chromed or powder-coated. Integrated armrests with plastic finish.; (W=570, D=600, H=865, SH=465)",
		"fr": "Siège visiteur. 4 pieds avec roulettes. Assise recouverte de tissu, dossier recouvert de maille. La structure en acier peut être chromée ou revêtue à la poudre. Accoudoirs intégrés avec finitions en plastique.; (W=570, D=600, H=865, SH=465)",
		"lt": "Lankytojo kėdė su keturiomis kojomis ir ratukais. Sėdimoji dalis aptraukta gobelenu, atlošas - tinkleliu. Plieninis rėmas gali būti chromuotas arba dažytas. Porankių viršus padengtas juodos spalvos plastiko padeliais. ; (W=570, D=600, H=865, SH=465)",
		"ru": "Стул для посетителей на 4 металлических ножках  на колесиках. Сидение обтянуто гобеленом, искусственной или натуральной кожей, спинка - сеткой. Стальная рама может быть хромированная или крашенная. Верхняя чаcть подлокотников покрыта накладкой из черного пластика.; (W=570, D=600, H=865, SH=465)",
		"us": "Visitor chair. Four legged with castors. Seat upholstered with fabric, back-rest with mesh. Steel frame can be chromed or powder-coated. Integrated armrests with plastic finish.; (W=570, D=600, H=865, SH=465)",
	}
	decoded := ofmlenc.DecodeMap(encoded)
	checkMapsEqual(t, decoded, expected)
}

// TestDecodeWindows1252Extra tests the case where there are unsupported languages in encoded map
func TestDecodeMapWindows1252Extra(t *testing.T) {
	encoded := map[string]string{
		"en": "Visitor chair. Four legged with castors. Seat upholstered with fabric, back-rest with mesh. Steel frame can be chromed or powder-coated. Integrated armrests with plastic finish.; (W=570, D=600, H=865, SH=465)",
		"de": "Besucherstuhl. Vierbein mit Rollen. Sitz gepolstert mit Stoff, Rückenlehne mit Mesh. Stahlrahmen kann verchromt oder pulverbeschichtet sein. Integrierte Armlehnen mit Kunststoffauflagen.; (W=570, D=600, H=865, SH=465)",
		"fr": "Siège visiteur. 4 pieds avec roulettes. Assise recouverte de tissu, dossier recouvert de maille. La structure en acier peut être chromée ou revêtue à la poudre. Accoudoirs intégrés avec finitions en plastique.; (W=570, D=600, H=865, SH=465)",
		"lt": "Lankytojo këdë su keturiomis kojomis ir ratukais. Sëdimoji dalis aptraukta gobelenu, atloðas - tinkleliu. Plieninis rëmas gali bûti chromuotas arba daþytas. Porankiø virðus padengtas juodos spalvos plastiko padeliais. ; (W=570, D=600, H=865, SH=465)",
		"ru": "Ñòóë äëÿ ïîñåòèòåëåé íà 4 ìåòàëëè÷åñêèõ íîæêàõ  íà êîëåñèêàõ. Ñèäåíèå îáòÿíóòî ãîáåëåíîì, èñêóññòâåííîé èëè íàòóðàëüíîé êîæåé, ñïèíêà - ñåòêîé. Ñòàëüíàÿ ðàìà ìîæåò áûòü õðîìèðîâàííàÿ èëè êðàøåííàÿ. Âåðõíÿÿ ÷àcòü ïîäëîêîòíèêîâ ïîêðûòà íàêëàäêîé èç ÷åðíîãî ïëàñòèêà.; (W=570, D=600, H=865, SH=465)",
		"us": "Visitor chair. Four legged with castors. Seat upholstered with fabric, back-rest with mesh. Steel frame can be chromed or powder-coated. Integrated armrests with plastic finish.; (W=570, D=600, H=865, SH=465)",
		"zh": "è®¿å®¢æ¤…ã€‚ å››è„šå¸¦è„šè½®ã€‚ åº§æ¤…é‡‡ç”¨ç»‡ç‰©è½¯åž«ï¼Œé� èƒŒé‡‡ç”¨ç½‘çœ¼ã€‚ é’¢åˆ¶æ¡†æž¶å�¯ä»¥é•€é“¬æˆ–ç²‰æœ«æ¶‚å±‚ã€‚ ä¸€ä½“å¼�æ‰¶æ‰‹ï¼Œå¸¦å¡‘æ–™é¥°é�¢ã€‚ ï¼ˆW = 570ï¼ŒD = 600ï¼ŒH = 865ï¼ŒSH = 465",
	}
	expected := map[string]string{
		"de": "Besucherstuhl. Vierbein mit Rollen. Sitz gepolstert mit Stoff, Rückenlehne mit Mesh. Stahlrahmen kann verchromt oder pulverbeschichtet sein. Integrierte Armlehnen mit Kunststoffauflagen.; (W=570, D=600, H=865, SH=465)",
		"en": "Visitor chair. Four legged with castors. Seat upholstered with fabric, back-rest with mesh. Steel frame can be chromed or powder-coated. Integrated armrests with plastic finish.; (W=570, D=600, H=865, SH=465)",
		"fr": "Siège visiteur. 4 pieds avec roulettes. Assise recouverte de tissu, dossier recouvert de maille. La structure en acier peut être chromée ou revêtue à la poudre. Accoudoirs intégrés avec finitions en plastique.; (W=570, D=600, H=865, SH=465)",
		"lt": "Lankytojo kėdė su keturiomis kojomis ir ratukais. Sėdimoji dalis aptraukta gobelenu, atlošas - tinkleliu. Plieninis rėmas gali būti chromuotas arba dažytas. Porankių viršus padengtas juodos spalvos plastiko padeliais. ; (W=570, D=600, H=865, SH=465)",
		"ru": "Стул для посетителей на 4 металлических ножках  на колесиках. Сидение обтянуто гобеленом, искусственной или натуральной кожей, спинка - сеткой. Стальная рама может быть хромированная или крашенная. Верхняя чаcть подлокотников покрыта накладкой из черного пластика.; (W=570, D=600, H=865, SH=465)",
		"us": "Visitor chair. Four legged with castors. Seat upholstered with fabric, back-rest with mesh. Steel frame can be chromed or powder-coated. Integrated armrests with plastic finish.; (W=570, D=600, H=865, SH=465)",
	}
	decoded := ofmlenc.DecodeMap(encoded)
	checkMapsEqual(t, decoded, expected)
}

func TestDecodeMapSimpleExtra(t *testing.T) {
	encoded := map[string]string{
		"en": "GAMA",
		"de": "GAMA",
		"zh": "GAMA",
	}
	expected := map[string]string{
		"en": "GAMA",
		"de": "GAMA",
	}
	decoded := ofmlenc.DecodeMap(encoded)
	checkMapsEqual(t, decoded, expected)
}

func TestDecode(t *testing.T) {
	encoded := "Ñòóë äëÿ ïîñåòèòåëåé íà 4 ìåòàëëè÷åñêèõ íîæêàõ  íà êîëåñèêàõ. Ñèäåíèå îáòÿíóòî ãîáåëåíîì, èñêóññòâåííîé èëè íàòóðàëüíîé êîæåé, ñïèíêà - ñåòêîé. Ñòàëüíàÿ ðàìà ìîæåò áûòü õðîìèðîâàííàÿ èëè êðàøåííàÿ. Âåðõíÿÿ ÷àcòü ïîäëîêîòíèêîâ ïîêðûòà íàêëàäêîé èç ÷åðíîãî ïëàñòèêà.; (W=570, D=600, H=865, SH=465)"
	expected := "Стул для посетителей на 4 металлических ножках  на колесиках. Сидение обтянуто гобеленом, искусственной или натуральной кожей, спинка - сеткой. Стальная рама может быть хромированная или крашенная. Верхняя чаcть подлокотников покрыта накладкой из черного пластика.; (W=570, D=600, H=865, SH=465)"
	decoded, err := ofmlenc.Decode("ru", encoded)
	if err != nil {
		t.Errorf("decoding error: %s", err.Error())
	}
	if decoded != expected {
		t.Errorf("decoded = %s; want %s", decoded, expected)
	}
}

func TestEncode(t *testing.T) {
	input := "Стул для посетителей на 4 металлических ножках  на колесиках. Сидение обтянуто гобеленом, искусственной или натуральной кожей, спинка - сеткой. Стальная рама может быть хромированная или крашенная. Верхняя чаcть подлокотников покрыта накладкой из черного пластика.; (W=570, D=600, H=865, SH=465)"
	expected := "Ñòóë äëÿ ïîñåòèòåëåé íà 4 ìåòàëëè÷åñêèõ íîæêàõ  íà êîëåñèêàõ. Ñèäåíèå îáòÿíóòî ãîáåëåíîì, èñêóññòâåííîé èëè íàòóðàëüíîé êîæåé, ñïèíêà - ñåòêîé. Ñòàëüíàÿ ðàìà ìîæåò áûòü õðîìèðîâàííàÿ èëè êðàøåííàÿ. Âåðõíÿÿ ÷àcòü ïîäëîêîòíèêîâ ïîêðûòà íàêëàäêîé èç ÷åðíîãî ïëàñòèêà.; (W=570, D=600, H=865, SH=465)"
	encoded, err := ofmlenc.Encode("ru", input)
	if err != nil {
		t.Errorf("encoding error: %s", err.Error())
	}
	if encoded != expected {
		t.Errorf("encoded = %s; want %s", encoded, expected)
	}
}

func TestDecodeErrors(t *testing.T) {
	_, err := ofmlenc.Decode("zh", "è®¿å®¢æ¤…ã€")
	if err != ofmlenc.ErrUnsupportedLang {
		t.Errorf("decoding unsupported language returned error %v; expected %s", err, ofmlenc.ErrUnsupportedLang)
	}

	_, err = ofmlenc.Decode("ru", "Стул")
	if err == nil {
		t.Errorf("decoding unencoded text returned no error; expected some error")
	}
}

func TestEncodeErrors(t *testing.T) {
	// try unsupported language
	_, err := ofmlenc.Encode("zh", "访客椅")
	if err != ofmlenc.ErrUnsupportedLang {
		t.Errorf("encoding unsupported language returned error %v; expected %s", err, ofmlenc.ErrUnsupportedLang)
	}

	// trying to encode a mix of languages
	_, err = ofmlenc.Encode("ru", "Стул kėdė")
	if err == nil {
		t.Errorf("expected an error encoding symbols from incompatible charsets")
	}
}

func checkMapsEqual(t *testing.T, decoded, expected map[string]string) {
	if decoded == nil {
		t.Errorf("decoded = nil")
		return
	}
	if len(decoded) != len(expected) {
		t.Errorf("len(decoded) = %d; want %d;\ndecoded = %#v\n expected = %#v", len(decoded), len(expected), decoded, expected)
	}
	for lang, d := range decoded {
		e, found := expected[lang]
		if !found {
			t.Errorf("decoded[%s] = %s; expected it to be unset", lang, d)
			continue
		}
		if d != e {
			t.Errorf("decoded[%s] != expected[%s]:\ndecoded[%s]  = %s\nexpected[%s] = %s", lang, lang, lang, d, lang, e)
		}
	}
}
